using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEditor;

public class CustomMenuWindow : EditorWindow
{
    [MenuItem("MyMenu/Custom Window")]
    static void ShowWindow()
    {
        CustomMenuWindow window = (CustomMenuWindow)EditorWindow.GetWindowWithRect(typeof(CustomMenuWindow), new Rect(0, 0, 750, 500), true);
        window.Show();
    }
    private void OnGUI()
    {
        ControlSelectionSection();
        SceneGenerationSection();
    }
    private void OnEnable()
    {
        EditorSceneManager.newSceneCreated += NewSceneSetupCallback;
        EditorSceneManager.sceneClosed += SceneClosedCallback;
    }

    private void SceneClosedCallback(Scene scene)
    {
        //alreadyInstantiated = false;
    }

    private void NewSceneSetupCallback(Scene scene, NewSceneSetup setup, NewSceneMode mode)
    {
        if(!alreadyInstantiated)
        {
            //alreadyInstantiated = true;
            GameObject character = Instantiate(Resources.Load("character", typeof(GameObject))) as GameObject;
            if (showStraightPathControl)
            {
                GameObject straightPath = Instantiate(Resources.Load("path", typeof(GameObject))) as GameObject;
            }
            else if (showCurvedPathControl)
            {
                GameObject curvedPath = Instantiate(Resources.Load("curvedPath", typeof(GameObject))) as GameObject;
            }
        }
        
    }

    private void ControlSelectionSection()
    {
        GUILayout.Label("Select Settings : ", EditorStyles.boldLabel);
        GUILayout.BeginArea(new Rect(25, 25, 500, 250));
        showControlSelection = EditorGUI.BeginFoldoutHeaderGroup(new Rect(10, 10, position.width, 20), showControlSelection, "Gameplay Control Select");
        if (showControlSelection)
        {
            bool isStrightControl = EditorGUI.Toggle(new Rect(25, 30, position.width, 20),
                                                    "Straight Path",
                                                     showStraightPathControl) ?
                                                     showCurvedPathControl = false :
                                                     showCurvedPathControl = true;
            bool isCurvedControl = EditorGUI.Toggle(new Rect(25, 50, position.width, 20),
                                                    "Curved Path",
                                                     showCurvedPathControl) ?
                                                     showStraightPathControl = false :
                                                     showStraightPathControl = true;

         

        }
        EditorGUILayout.EndFoldoutHeaderGroup();
        GUILayout.EndArea();
    }

    private void SceneGenerationSection()
    {
        if(GUI.Button(new Rect(position.width - 135, position.height - 35, 125, 25), "Generate Scene"))
        {
            GenerateScene();

        }
    }

    private static void GenerateScene()
    {
        Scene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);
        var savePath = "Assets/Scenes/NewScene.unity";
        EditorSceneManager.SaveScene(newScene, savePath);
        EditorSceneManager.OpenScene(savePath, OpenSceneMode.Single);
        GUIUtility.ExitGUI();
    }

    private void OnFocus()
    {
        if (EditorPrefs.HasKey("StraightPathControl"))
            showStraightPathControl = EditorPrefs.GetBool("StraightPathControl");
        if (EditorPrefs.HasKey("CurvedPathControl"))
            showCurvedPathControl = EditorPrefs.GetBool("CurvedPathControl");
    }
    private void OnLostFocus()
    {
        EditorPrefs.SetBool("StraightPathControl", showStraightPathControl);
        EditorPrefs.SetBool("CurvedPathControl", showCurvedPathControl);
    }
    private void OnDestroy()
    {
        EditorPrefs.SetBool("StraightPathControl", showStraightPathControl);
        EditorPrefs.SetBool("CurvedPathControl", showCurvedPathControl);
    }
    #region Private Variables
    private bool showControlSelection = false;
    private bool showStraightPathControl = true;
    private bool showCurvedPathControl = false;
    private bool alreadyInstantiated = false;
    #endregion
}